#!/usr/bin/env python3

import glob
import hashlib
import os
import pprint
from optparse import OptionParser
from stat import S_ISDIR, S_ISLNK

descr = """Find duplicated files recursively in a given directory tree.
Files sizes are checked first, then a chksum is performed to resolve
ambiguities. The list of duplicated files by size and by full check
is given in two output files with self-explicative names.
"""

parser = OptionParser(usage = '%prog [options]', version = 'v0.1', description = descr)
parser.add_option('-d', '--dir', dest = 'dir', default = './', help = 'input directory for duplicated file search, [default=%default]')
parser.add_option('-o', '--out', dest = 'ofile', default = 'duplicated_files.list', help = 'duplicated file list, [default=%default]')
parser.add_option('-s', '--out-by-size', dest = 'ofile_size', default = 'duplicated_by_size.list', help = 'duplicated file list based on file size only, [default=%default]')
(options, args) = parser.parse_args()

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

g = glob.glob(options.dir + "/**", recursive=True)

nfiles = len(g)

# find duplicates by size
cnt = 0
data = {}
for f in g:
    s = os.stat(f, follow_symlinks=False)
    if S_ISDIR(s.st_mode) or S_ISLNK(s.st_mode):
        continue
    #print(nfiles, f, s.st_size, md5(f))
    if cnt % 137 == 0:
        print("%.1f" % (100 * cnt / nfiles))
    data[f] = s.st_size
    cnt += 1

rev_multidict = {}
for key, value in data.items():
    rev_multidict.setdefault(value, set()).add(key)

dupval = [values for key, values in rev_multidict.items() if len(values) > 1]

of = open(options.ofile_size, 'w')
pprint.pprint(dupval, stream=of)
of.close()

ndups = len(dupval)

# refine duplicates by chksum
data_md5 = {}
cnt = 0
for fl in dupval:
    for f in fl:
        data_md5[f] = md5(f)
    print("%6d/%d %.1f" % (cnt, ndups, 100 * cnt / ndups))
    cnt += 1

rev_multidict = {}
for key, value in data_md5.items():
    rev_multidict.setdefault(value, set()).add(key)

of = open(options.ofile, 'w')
print("# file format: { comma-separated list of duplicated file names } md5sum", file=of)
for key, values in rev_multidict.items():
    if len(values) > 1:
        print(values, key, file=of)
of.close()
