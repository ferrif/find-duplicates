## Find duplicates

Find duplicated files recursively in a given directory tree.
Files sizes are checked first, then a chksum is performed to resolve
ambiguities. The list of duplicated files by size and by full check
is given in two output files with self-explicative names.
